import pandas
import sys
import os
import json
from PIL import Image

# Amount of data to be used for validation during training
TEST_SPLIT = 0.1

if len(sys.argv) < 3:
    print("""Usage:
    %s <input_dir> <output_dir>
please make sure input_dir != output_dir""", file=sys.stderr)
    exit(-1)


# This directory should contain Images and Labels
inDir = sys.argv[1]

# This directory will contain the generated output
outDir = sys.argv[2]

if inDir[-1] == '/':
    inDir = inDir[:-1]

if outDir[-1] == '/':
    outDir = outDir[:-1]

if inDir == outDir:
    print("input and output directory are the same, this is not allowed!", file=sys.stderr)
    exit(-1)

labelDir = "%s/Labels" % inDir
imgDir = "%s/Images" % inDir

classesById = {}

entries = []

for fentry in os.listdir(labelDir):
    fname = "%s/%s" % (labelDir, fentry)

    if fentry == 'classes.txt':
        with open(fname) as f:
            for i, line in enumerate(f.readlines()):
                classesById[i+1] = line.strip()
        continue

    imgName = "%s/%s.png" % (imgDir, ".".join(fentry.split(".")[:-1]))
    img = Image.open(imgName)

    width, height = img.size

    with open(fname) as f:
        for line in f.readlines():
            entry = {
                "filename": os.path.abspath(imgName),
                "width": width,
                "height": height,
            }
            ls = line.split()
            rect = [float(e) for e in ls[1:]]
            entry['class_id'] = int(ls[0])+1
            entry['xmin'] = int(width * float(ls[1]))
            entry['ymin'] = int(height * float(ls[2]))
            entry['xmax'] = int(width * float(ls[3])) + entry['xmin']
            entry['ymax'] = int(height * float(ls[4])) + entry['ymin']

            entries.append(entry)

for e in entries:
    e['class'] = classesById[e['class_id']]

dataValues = [(e['filename'], e['width'], e['height'], e['class'], e['xmin'], e['ymin'], e['xmax'], e['ymax']) for e in entries]
df = pandas.DataFrame(dataValues, columns=['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax'])
test_df = df.sample(int(TEST_SPLIT * len(df)))
train_df = df.drop(test_df.index)

print(train_df)

# Now all data is present, it just needs to be moved

os.system("mkdir -p %s" % outDir)
train_df.to_csv('%s/train_data.csv' % outDir, index=None)
test_df.to_csv('%s/test_data.csv' % outDir, index=None)

with open('%s/label_map.pbtxt' % outDir, 'w') as out:
    for i in classesById.keys():
        print('item {', file=out)
        print('  id: %i' % i, file=out)
        print("  name: '%s'" % classesById[i], file=out)
        print('}', file=out)
        print('', file=out)

with open('%s/py_label_map.json' % outDir, 'w') as jout:
    jout.write(json.dumps(classesById))
