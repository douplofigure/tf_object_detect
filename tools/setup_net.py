import sys
import os
import tarfile
import re


inDir = sys.argv[1]
if inDir[-1] == '/':
    inDir = inDir[:-1]

MODEL_DIR = os.path.abspath('%s/models/' % inDir)
ROOT_DIR = os.path.abspath(os.getcwd())
DATA_DIR = "%s/%s" % (ROOT_DIR, inDir)
MODEL_ZOO_DIR = "%s/models" % ROOT_DIR

test_record_fname = '%s/test_data.record' % DATA_DIR
train_record_fname = '%s/train_data.record' % DATA_DIR
label_map_pbtxt_fname = '%s/label_map.pbtxt' % DATA_DIR


num_steps = 4000
num_eval_steps = 500

model_name = 'ssd_mobilenet_v2_320x320_coco17_tpu-8'
#model_name = 'ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8'
pretrained_checkpoint = '%s.tar.gz' % model_name
base_pipeline_file = '%s/pipeline.config' % model_name
batch_size = 16

os.system('mkdir -p %s' % MODEL_DIR)
os.chdir(MODEL_DIR)

if pretrained_checkpoint not in os.listdir(MODEL_DIR):
    download_tar = 'http://download.tensorflow.org/models/object_detection/tf2/20200711/' + pretrained_checkpoint
    os.system("wget %s" % download_tar)

    tar = tarfile.open(pretrained_checkpoint)
    tar.extractall()
    tar.close()

#prepare
pipeline_fname = MODEL_DIR + '/' + base_pipeline_file
fine_tune_checkpoint = MODEL_DIR + '/' + model_name + '/checkpoint/ckpt-0'


def get_num_classes(pbtxt_fname):
    from object_detection.utils import label_map_util
    label_map = label_map_util.load_labelmap(pbtxt_fname)
    categories = label_map_util.convert_label_map_to_categories(
        label_map, max_num_classes=90, use_display_name=True)
    category_index = label_map_util.create_category_index(categories)
    return len(category_index.keys())


num_classes = get_num_classes(label_map_pbtxt_fname)

os.chdir(MODEL_DIR)
print('writing custom configuration file')

with open(pipeline_fname) as f:
    s = f.read()
with open('pipeline_file.config', 'w') as f:

    # fine_tune_checkpoint
    s = re.sub('fine_tune_checkpoint: ".*?"',
               'fine_tune_checkpoint: "{}"'.format(fine_tune_checkpoint), s)

    # tfrecord files train and test.
    s = re.sub(
        '(input_path: ".*?)(PATH_TO_BE_CONFIGURED)(.*?")', 'input_path: "{}"'.format(train_record_fname), s)
    s = re.sub(
        '(input_path: ".*?)(PATH_TO_BE_CONFIGURED)(.*?")', 'input_path: "{}"'.format(test_record_fname), s)

    # label_map_path
    s = re.sub(
        'label_map_path: ".*?"', 'label_map_path: "{}"'.format(label_map_pbtxt_fname), s)

    # Set training batch_size.
    s = re.sub('batch_size: [0-9]+',
               'batch_size: {}'.format(batch_size), s)

    # Set training steps, num_steps
    s = re.sub('num_steps: [0-9]+',
               'num_steps: {}'.format(num_steps), s)

    # Set number of classes num_classes.
    s = re.sub('num_classes: [0-9]+',
               'num_classes: {}'.format(num_classes), s)

    #fine-tune checkpoint type
    s = re.sub(
        'fine_tune_checkpoint_type: "classification"', 'fine_tune_checkpoint_type: "{}"'.format('detection'), s)

    f.write(s)

pipeline_file = '%s/pipeline_file.config' % MODEL_DIR
model_dir = MODEL_DIR

for i in range(10):
    print("")
print("Command to run to start training:")
print("\tpython3 %s/research/object_detection/model_main_tf2.py --pipeline_config_path=%s --model_dir=%s --alsologtostderr --num_train_steps=%i --sample_1_of_n_eval_examples=1 --num_eval_steps=%s" % (MODEL_ZOO_DIR, pipeline_file, model_dir, num_steps, num_eval_steps))
