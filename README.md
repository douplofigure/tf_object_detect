# Setting up venv

	python3 -m virtualenv env
	source env/bin/activate


# Installing requirements

To install the required packages run:

	pip install -r requirements.txt
	git clone --depth 1 https://github.com/tensorflow/models
	cd models/research
	protoc object_detection/protos/*.proto --python_out=.
	cp object_detection/packages/tf2/setup.py .
	python -m pip install .
	
# Convert data from old type to tf-records

	python3 tools/label2csv.py <orig_dir> <train_data_dir>
	python3 tools/csv2tfrecord.py <train_data_dir>
	
In "train_data_dir" you should now have 6 files:
 
- label_map.pbtxt
- py_label_map.json
- test_data.csv
- test_data.record
- train_data.csv
- train_data.record

# Setup training environement

	python3 tools/setup_net.py <train_data_dir>

This should setup your training data directory and download the specified model.
This will print a command to execute to start the training, based on the specified net and data.
